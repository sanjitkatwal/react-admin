import './App.css';
import {BrowserRouter as Router, Switch, Route, Routes} from "react-router-dom";
import SidebarComponent from "./components/sidebar/Sidebar.component";
import ProductList from "./components/pages/productList/ProductList";
import TopbarComponent from "./components/topbar/Topbar.component";
import HomeComponent from "./components/pages/home/Home.component";
import NewProduct from "./components/pages/newProduct/NewProduct";
import UserList from "./components/pages/userList/UserList";
import NewUser from "./components/pages/newUser/NewUser";
import Product from "./components/pages/product/Product";
import User from "./components/pages/user/User";

function App() {

  return (
    <Router>
      <TopbarComponent />
      <div className="container">
        <SidebarComponent />
          <Routes>
              <Route exact path="/" element={<HomeComponent /> }/>
              <Route path="/users" element={<UserList /> } />
              <Route path="/user/:userId" element={<User /> } />
              <Route path="/newUser" element={<NewUser /> } />
              <Route path="/products" element={<ProductList /> } />
              <Route path="/products/:productsId" element={<Product /> } />
              <Route path="/newProduct" element={<NewProduct /> } />
          </Routes>
      </div>
    </Router>
  );
}

export default App;
