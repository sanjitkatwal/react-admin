import React from 'react';
import "./newUser.css"
function NewUser(props) {
    return (
        <div className="newUser">
            <h1 className="newUserTitle">New User</h1>
            <form action="" className="newUserForm">
                <div className="newUserItem">
                    <label htmlFor="">User Name</label>
                    <input type="text" placeholder="Enter User Name"/>
                </div>
                <div className="newUserItem">
                    <label htmlFor="">Full Name</label>
                    <input type="text" placeholder="Enter Full Name"/>
                </div>
                <div className="newUserItem">
                    <label htmlFor="">Email</label>
                    <input type="email" placeholder="Enter Email"/>
                </div>
                <div className="newUserItem">
                    <label htmlFor="">Password</label>
                    <input type="password" placeholder="Enter Password"/>
                </div>
                <div className="newUserItem">
                    <label htmlFor="">phone</label>
                    <input type="text" placeholder="Enter Phone Number"/>
                </div>
                <div className="newUserItem">
                    <label htmlFor="">Address</label>
                    <input type="text" placeholder="Enter Address"/>
                </div>
                <div className="newUserItem">
                    <label htmlFor="">Gender</label>
                    <div className="newUserGender">
                        <input type="radio" name="gender" value="male" id="male"/>
                        <label htmlFor="male">Male</label>
                        <input type="radio" name="gender" value="female" id="female"/>
                        <label htmlFor="female">Female</label>
                        <input type="radio" name="gender" value="other" id="other"/>
                        <label htmlFor="other">Other</label>
                    </div>
                </div>
                <div className="newUserItem">
                    <label htmlFor="">Active</label>
                    <select name="active" id="active" className="newUserSelect">
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
                <button className="newUserButton">Create</button>
            </form>
        </div>
    );
}

export default NewUser;