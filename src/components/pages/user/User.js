import React from 'react';
import "./user.css"
import {CalendarToday, LocationSearching, MailOutline, PermIdentity, PhoneAndroid, Publish} from "@material-ui/icons";
import {Link} from "react-router-dom";


function User(props) {
    return (
        <div className="user">
            <div className="userTitleContainer">
                <h1 className="userTitle">Edit User</h1>
                <Link to="/newUser">
                    <button className="userAddButton">Create</button>
                </Link>
            </div>
            <div className="userContainer">
                <div className="userShow">
                    <div className="userShowTop">
                        <img className="userShowImg" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRAyJtsX5lnJz8x7inT4CJsUbqAHlua5qhMKQ&usqp=CAU" alt=""/>
                       <div className="userShowTopTitle">
                           <span className="userShowUsername">Royce Bibber</span>
                           <span className="userShowUserTitle">Software engineering</span>
                       </div>
                    </div>
                    <div className="userShowBottom">
                        <span className="userShowTitle">Account Details</span>
                        <div className="userShowInfo">
                            <PermIdentity className="userShowIcon" />
                            <span className="userShowInfoTitle">annabeck99</span>
                        </div>
                        <div className="userShowInfo">
                            <CalendarToday className="userShowIcon" />
                            <span className="userShowInfoTitle">11.12.2009</span>
                        </div>
                        <span className="userShowTitle">Contact Details</span>
                        <div className="userShowInfo">
                            <PhoneAndroid className="userShowIcon" />
                            <span className="userShowInfoTitle">+977:9849158572</span>
                        </div>
                        <div className="userShowInfo">
                            <MailOutline className="userShowIcon" />
                            <span className="userShowInfoTitle">annabeck99@gmail.com</span>
                        </div>
                        <div className="userShowInfo">
                            <LocationSearching className="userShowIcon" />
                            <span className="userShowInfoTitle">Kathmandu, Nepal</span>
                        </div>
                    </div>
                </div>
                <div className="userUpdate">
                    <span className="userUpdateTitle">Edit</span>
                    <form action="" className="userUpdateForm">
                        <div className="userUpdateLeft">
                            <div className="userUpdateItem">
                                <label htmlFor="userName">User Name</label>
                                <input type="text" placeholder="username" className="userUpdateInput"/>
                            </div>
                            <div className="userUpdateItem">
                                <label htmlFor="userName">Full Name</label>
                                <input type="text" placeholder="Enter Full Name" className="userUpdateInput"/>
                            </div>
                            <div className="userUpdateItem">
                                <label htmlFor="userName">Email</label>
                                <input type="text" placeholder="Enter Email" className="userUpdateInput"/>
                            </div>
                            <div className="userUpdateItem">
                                <label htmlFor="userName">Phone</label>
                                <input type="text" placeholder="Enter Phone Number" className="userUpdateInput"/>
                            </div>
                            <div className="userUpdateItem">
                                <label htmlFor="userName">Address</label>
                                <input type="text" placeholder="Enter Address" className="userUpdateInput"/>
                            </div>

                        </div>
                        <div className="userUpdateRight">
                            <div className="userUpdateUpload">
                                <img className="userUpdateImg" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRpN8UsyTVSt3An9X1qCk9HcAewop-QfMZZBg&usqp=CAU" alt=""/>
                                <label htmlFor="file"><Publish className="userUpdateIcon" /></label>
                                <input type="file" id="file" style={{display:"none"}}/>
                            </div>
                            <button className="userUpdateButton">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default User;