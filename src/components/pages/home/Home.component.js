import React from 'react';
import "./home.component.css";
import FeatureInfoComponent from "../../featuredInfo/FeatureInfo.component";
import ChartComponent from "../../chart/Chart.component";
import {userData} from "../../../dummyData";
import WidgetsSmComponent from "../../widgetSm/WidgetsSm.component";
import WidgetsLgComponent from "../../widgetLg/WidgetsLg.component";

function HomeComponent() {
    return (
        <div className="home">
            <FeatureInfoComponent />
            <ChartComponent data={userData} title="User Analytics" grid dataKey="Active User" />
            <div className="homeWidgets">
                <WidgetsSmComponent />
                <WidgetsLgComponent />
            </div>
        </div>
    );
}

export default HomeComponent;