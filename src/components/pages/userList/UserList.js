import React, {useState} from 'react';
import "./userlist.css";
import { DataGrid } from '@mui/x-data-grid';
import {DeleteOutline} from "@material-ui/icons";
import {userRows} from "../../../dummyData";
import {Link} from "react-router-dom";

function UserList(props) {
    const [data, setData] = useState(userRows);

    const handleDelete = (id)=>{
       setData(data.filter(item=>item.id !==id));
    }

    const columns = [
        { field: 'id', headerName: 'ID', width: 70 },
        { field: 'user', headerName: 'Username', width: 200, renderCell:(params)=>{
            return (
                <div className="userListUser">
                    <img className='userListImage' src={params.row.avatar} alt="no images"/>
                    {params.row.username}
                </div>
            )
            } },
        {field: 'email', headerName: 'Email', width: 200 },
        {field: 'status', headerName: 'Status', width: 90,},
        {field: 'transaction', headerName: 'Transaction volume', width: 160},
        {field: 'action', headerName: 'Action', width: 150, renderCell:(params)=>{
            return(
                <>
                    <Link to={"/user/"+params.row.id}>
                        <button className="userListEdit">Edit</button>
                    </Link>
                    <DeleteOutline className="userListDelete" onClick={()=>handleDelete(params.row.id)} />
                </>
            )
            }}
    ];


    return (
        <div className="userList">
            <DataGrid
                rows={data} disableSelectionOnClick columns={columns} pageSize={10} rowsPerPageOptions={[5]} checkboxSelection
            />
        </div>
    );
}

export default UserList;