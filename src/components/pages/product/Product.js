import React from 'react';
import "./product.css";
import {Link} from "react-router-dom";
import ChartComponent from "../../chart/Chart.component";
import {productData} from '../../../dummyData'
import {Publish} from "@material-ui/icons";

function Product(props) {
    return (
        <div className="product">
            <div className="productTitleContainer">
                <h1 className="productTitle">Product</h1>
                <Link to="/newProduct">
                    <button className="productAddButton">Create</button>
                </Link>
            </div>
            <div className="productTop">
                <div className="productTopLeft">
                    <ChartComponent data={productData} dataKey="Sales" title="Sales Performance" />
                </div>
                <div className="productTopRight">
                   <div className="productInfoTop">
                       <img className="productInfoImage"  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRgK8SZTmKteTuSFgK-gBVMNDpV2mX8ka7A3A&usqp=CAU" alt=""/>
                       <span className="productName">Apple Airpods</span>
                   </div>
                   <div className="productInfoBottom">
                       <div className="productInfoItem">
                           <span className="productInfoKey">Id:</span>
                           <span className="productInfoValue">123</span>
                       </div>
                       <div className="productInfoItem">
                           <span className="productInfoKey">Sales:</span>
                           <span className="productInfoValue">12233</span>
                       </div>
                       <div className="productInfoItem">
                           <span className="productInfoKey">Active:</span>
                           <span className="productInfoValue">Yes</span>
                       </div>
                       <div className="productInfoItem">
                           <span className="productInfoKey">In Stock:</span>
                           <span className="productInfoValue">No</span>
                       </div>
                   </div>
                </div>
            </div>
            <div className="productContainer">
                <form action="" className="productForm">
                    <div className="productFormLeft">
                        <label htmlFor="productName">Product Name</label>
                        <input type="text" placeholder="Apple AirPod"/>
                        <label>In Stock</label>
                        <select name="inStock" id="inStock">
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                        <label>Active</label>
                        <select name="active" id="active">
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                    <div className="productFormRight">
                        <div className="productUpload">
                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRgK8SZTmKteTuSFgK-gBVMNDpV2mX8ka7A3A&usqp=CAU" alt="" className="productUploadImg"/>
                            <label for="file">
                                <Publish />
                            </label>
                            <input type="file" id="file" style={{display:"none"}}/>
                        </div>
                        <button className="productButton">Update</button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Product;