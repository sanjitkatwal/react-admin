import React from 'react';
import "./feactureInfo.component.css"
import {ArrowDownward, ArrowUpward} from "@material-ui/icons"

function FeatureInfoComponent(props) {
    return (
        <div className="featured">
            <div className="featuredItem">
               <span className="featuredTitle">Revanue</span>
                <div className="featuredMoneyContainer">
                    <span className="featuredMoney">$2,232</span>
                    <span className="featuredMoneyRate">-32.22 <ArrowDownward className="featuredIcon negative"  /> </span>
                </div>
                <span className="featuredSub">
                    Compare to last month
                </span>
            </div>

            <div className="featuredItem">
                <span className="featuredTitle">Revanue</span>
                <div className="featuredMoneyContainer">
                    <span className="featuredMoney">$2,232</span>
                    <span className="featuredMoneyRate">-2.22 <ArrowDownward className="featuredIcon negative" /> </span>
                </div>
                <span className="featuredSub">
                    Compare to last month
                </span>
            </div>

            <div className="featuredItem">
                <span className="featuredTitle">Cost</span>
                <div className="featuredMoneyContainer">
                    <span className="featuredMoney">$2,432</span>
                    <span className="featuredMoneyRate">+2.22 <ArrowUpward  className="featuredIcon"  /> </span>
                </div>
                <span className="featuredSub">
                    Compare to last month
                </span>
            </div>
        </div>
    );
}

export default FeatureInfoComponent;