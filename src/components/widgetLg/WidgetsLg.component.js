import React from 'react';
import './WidgetsLg.component.css'

function WidgetsLgComponent(props) {
    const Button = (({type}) => {
        return <button className={"widgetLgButton " + type }>{type}</button>
    })
    return (
        <div className="widgetsLg">
            <h3 className="widgetLgTitle">Latest Transactions</h3>
            <table className="widgetLgTable">
                <tr className="widgetLgTr">
                    <th className="widgetLgTh">Customer</th>
                    <th className="widgetLgTh">Date</th>
                    <th className="widgetLgTh">Amount</th>
                    <th className="widgetLgTh">Status</th>
                </tr>
                <tr className="widgetLgTr">
                    <td className="widgetLgUser">
                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZW_hmPGvpEhTi7bLpyqGGh_5T-3yrHjlplg&usqp=CAU" alt="" className="widgetLgImg"/>
                        <span className="widgetLgName">Susan Carol</span>
                    </td>
                    <td className="widgetLgDate">2 June 2021</td>
                    <td className="widgetLgAmount">$228890</td>
                    <td className="widgetLgStatus"><Button type="Declined" /></td>
                </tr>

                <tr className="widgetLgTr">
                    <td className="widgetLgUser">
                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZW_hmPGvpEhTi7bLpyqGGh_5T-3yrHjlplg&usqp=CAU" alt="" className="widgetLgImg"/>
                        <span className="widgetLgName">Susan Carol</span>
                    </td>
                    <td className="widgetLgDate">2 June 2021</td>
                    <td className="widgetLgAmount">$228890</td>
                    <td className="widgetLgStatus"><Button type="Pending" /></td>
                </tr>

                <tr className="widgetLgTr">
                    <td className="widgetLgUser">
                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZW_hmPGvpEhTi7bLpyqGGh_5T-3yrHjlplg&usqp=CAU" alt="" className="widgetLgImg"/>
                        <span className="widgetLgName">Susan Carol</span>
                    </td>
                    <td className="widgetLgDate">2 June 2021</td>
                    <td className="widgetLgAmount">$228890</td>
                    <td className="widgetLgStatus"><Button type="Approved" /></td>
                </tr>

                <tr className="widgetLgTr">
                    <td className="widgetLgUser">
                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZW_hmPGvpEhTi7bLpyqGGh_5T-3yrHjlplg&usqp=CAU" alt="" className="widgetLgImg"/>
                        <span className="widgetLgName">Susan Carol</span>
                    </td>
                    <td className="widgetLgDate">2 June 2021</td>
                    <td className="widgetLgAmount">$228890</td>
                    <td className="widgetLgStatus"><Button type="Pending" /></td>
                </tr>

                <tr className="widgetLgTr">
                    <td className="widgetLgUser">
                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZW_hmPGvpEhTi7bLpyqGGh_5T-3yrHjlplg&usqp=CAU" alt="" className="widgetLgImg"/>
                        <span className="widgetLgName">Susan Carol</span>
                    </td>
                    <td className="widgetLgDate">2 June 2021</td>
                    <td className="widgetLgAmount">$228890</td>
                    <td className="widgetLgStatus"><Button type="Declined" /></td>
                </tr>
            </table>
        </div>
    );
}

export default WidgetsLgComponent;