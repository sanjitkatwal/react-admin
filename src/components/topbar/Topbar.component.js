import React from 'react';
import "./topbar.component.css"
import {NotificationsNone, Language, settings, Settings} from '@material-ui/icons';

function TopbarComponent(props) {
    return (
        <div className="topbar">
            <div className="topbarWrapper">
                <div className="topLeft">
                    <span className="logo">SK Admin</span>
                </div>
                <div className="topRight">
                    <div className="topbarIconContainer">
                        <NotificationsNone />
                        <span className="topIconBadge">20</span>
                    </div>
                    <div className="topbarIconContainer">
                        <Language />
                        <span className="topIconBadge">30</span>
                    </div>
                    <div className="topbarIconContainer">
                        <Settings />
                    </div>
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQBI8iylDwNy8B3hj4SfExMouiJ2Bp2LXVlKQ&usqp=CAU" alt="" className="topAvatar"/>
                </div>
            </div>
        </div>
    );
}

export default TopbarComponent;