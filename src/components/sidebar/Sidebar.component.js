import React from 'react';
import "./sidebar.component.css";

import {
    LineStyle,
    Mail,
    Message,
    Money,
    PermIdentity,
    Report,
    Storefront, Theaters,
    Timeline,
    TrendingUp, Work
} from "@material-ui/icons"
import {Badge} from "@material-ui/core";
import {Link} from "react-router-dom";

function SidebarComponent(props) {
    return (
        <div className="sidebar">
            <div className="sidebarWrapper">
                <div className="sidebarMenu">
                    <h3 className="sidebarTitle">Dashboard</h3>
                    <ul className="sidebarList">
                        <Link to="/" className="link">
                            <li className="sidebarListItem active">
                                <LineStyle className="sidebarIcon" /> Home
                            </li>
                        </Link>
                        <li className="sidebarListItem">
                            <Timeline className="sidebarIcon" /> Analytics
                        </li>
                        <li className="sidebarListItem">
                            <TrendingUp className="sidebarIcon" /> Sales
                        </li>
                    </ul>
                </div>

                <div className="sidebarMenu">
                    <h3 className="sidebarTitle">Quick Menu</h3>
                    <ul className="sidebarList">
                        <Link to="/users" className="link">
                            <li className="sidebarListItem">
                                <PermIdentity className="sidebarIcon" /> Users
                            </li>
                        </Link>
                        <Link to="/products" className="link">
                            <li className="sidebarListItem">
                                <Storefront className="sidebarIcon" /> Products
                            </li>
                        </Link>
                        <li className="sidebarListItem">
                            <Money className="sidebarIcon" /> Transactions
                        </li>
                        <li className="sidebarListItem">
                            <Report className="sidebarIcon" /> Reports
                        </li>
                    </ul>
                </div>

                <div className="sidebarMenu">
                    <h3 className="sidebarTitle">Notifications</h3>
                    <ul className="sidebarList">
                        <li className="sidebarListItem">
                            <Mail className="sidebarIcon" /> Mail
                        </li>
                        <li className="sidebarListItem">
                            <Timeline className="sidebarIcon" /> Feedback
                        </li>
                        <li className="sidebarListItem">
                            <Message className="sidebarIcon" /> Message
                        </li>
                    </ul>
                </div>


                <div className="sidebarMenu">
                    <h3 className="sidebarTitle">Staff</h3>
                    <ul className="sidebarList">
                        <li className="sidebarListItem">
                            <Work className="sidebarIcon" /> Manage
                        </li>
                        <li className="sidebarListItem">
                            <LineStyle className="sidebarIcon" /> Analytics
                        </li>
                        <li className="sidebarListItem">
                            <TrendingUp className="sidebarIcon" /> Reports
                        </li>
                    </ul>
                </div>

                <div className="sidebarMenu">
                    <h3 className="sidebarTitle">Settings</h3>
                    <ul className="sidebarList">
                        <li className="sidebarListItem">
                            <LineStyle className="sidebarIcon" /> Home
                        </li>
                        <li className="sidebarListItem">
                            <Theaters className="sidebarIcon" /> Change Theme
                        </li>
                        <li className="sidebarListItem">
                            <TrendingUp className="sidebarIcon" /> Others
                        </li>
                    </ul>
                </div>


            </div>
        </div>
    );
}

export default SidebarComponent;